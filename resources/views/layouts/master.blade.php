<!-- Stored in app/views/layouts/master.blade.php -->


<!DOCTYPE html>
<html>
<head>
	<meta name="_token" content="{!! csrf_token() !!}" /> 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		var number = 0;
		function add(){
			number++;
			document.getElementById('txt_numberdisplay').innerHTML = number;
		}

		function subtract(){
			number--;
			document.getElementById('txt_numberdisplay').innerHTML = number;
		}

		function reset(){
			number=0;
			document.getElementById('txt_numberdisplay').innerHTML = number;
		}
			
</script>
<script type="text/javascript">
	$.ajax({
			type: "POST",
		    url: '/insert', // Not sure what to add as URL here
		    data: { 
                    numberdisplay: number,
            }   
            success: function( number_input ) { 
                if( number_input ) { 
                    alert('OK');
                }   
            },  
            fail: function() {
                alert('NO');
            }   
         }); 
</script>
<style type="text/css">
	.box{
		height: 300px;
		width:600px;
		position: absolute;
		left: 28%;
		top: 27%;
		border:1px solid #ccc;
	}

	h1 {
		color: black;
		font-family: verdana;
		font-size: 300%;
	}


	p  {
		color: black;
		font-family: courier;
		font-size: 160%;
	}

	textarea {
		white-space: normal;
		text-align: justify;
		-moz-text-align-last: center; /* Firefox 12+ */
		text-align-last: center;
	}

	.formfield * {
		vertical-align: middle;
	}

	.addbutton {
		display: flex; 
		justify-content: center;
	}

	.addbutton .addbtn{
		position: absolute;
		top: 70%;
		left: 59%;
		transform: translate(-50%, -50%);
		-ms-transform: translate(-50%, -50%);
		background-color: #f1f1f1;
		color: black;
		font-size: 16px;
		padding: 7px 20px;
		border: none;
		cursor: pointer;
		border-radius: 5px;
		text-align: center;
	}

	.addbutton .addbtn:hover {
		background-color: black;
		color: white;
	}

	.subtractbutton {
		display: flex; 
		justify-content: center;
	}

	.subtractbutton .subtractbtn{
		position: absolute;
		top: 70%;
		left: 75%;
		transform: translate(-50%, -50%);
		-ms-transform: translate(-50%, -50%);
		background-color: #f1f1f1;
		color: black;
		font-size: 16px;
		padding: 7px 20px;
		border: none;
		cursor: pointer;
		border-radius: 5px;
		text-align: center;
	}

	.subtractbutton .subtractbtn:hover {
		background-color: black;
		color: white;
	}

	.resetbutton {
		position:relative;
		margin-top:80px;
		left: 25.5%;
	}

	.resetbutton .resetbtn{
		transform: translate(-50%, -50%);
		-ms-transform: translate(-50%, -50%);
		background-color: #f1f1f1;
		color: black;
		font-size: 16px;
		padding: 7px 20px;
		border: none;
		cursor: pointer;
		border-radius: 5px;

	}

	.resetbutton .resetbtn:hover {
		background-color: black;
		color: white;
	}
}

</style>
</head>
<body>
	<div align="center" class="container box">
		
		<h1>Laravel Code Testing</h1>

		<p>"Increment/Decrement Number"</p>

		<p class="formfield">

			<label for="txt_numberdisplay"> Current Value: </label>

			<textarea readonly rows="1" cols="20" name="txt_number" id="txt_numberdisplay"><?php echo $number ?? '';?></textarea>
		</p>

		<p class="addbutton">

			<button type="submit" name="add" onclick="add()" class="addbtn">+</button>

		</p>

		<p class="subtractbutton">

			<button type="submit" name="subtract" onclick="subtract()"class="subtractbtn">-</button>

		</p>

		<p class="resetbutton">

			<button type="submit" name="reset" onclick="reset()"class="resetbtn">Reset</button>

		</p>

		
	</div>
</body>


</html>
