<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

class CountController extends Controller
{
	public function insertNo(){
		return view('welcome');
	}


	public function insert(Request $request){
		$number_input = $request->input('number_display');

		$data=array('stored_value'=>$number_input);

		DB::table('numbers')->insert($data);
	}
}